# FAIR-Data Community Samenwerken in de Ondergrond (SidO)

## Officiele repository 
Officiele repository voor de data plan template, metadata template, FAIR-evalautor van de SidO FAIR-Data community is https://gitlab.com/fair-data-communities/sido/assets

## Omschrijving
Deze repository bevat de open source assets voor data aanbieders van de FAIR data community 'SidO' (Samenwerken in de ondegrond).De oorspronkelijke locatie van deze repository is https://gitlab.com/fair-data-communities/sido/assets

## SidO Data plan template Assets
Het data plan template is gemaakt in JSON. De template bevat de SidO Community specifieke data plan vragen omtrent assets. Je kan deze file gebruiken als seeding file en als databron. Locatie: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/dataplan_templates/assets/dataplan_template.json 

## SidO Data plan template Planningen
Het data plan template is gemaakt in JSON. De template bevat de SidO Community specifieke data plan vragen omtrent planningen. Je kan deze file gebruiken als seeding file en als databron. Locatie: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/dataplan_templates/planning/dataplan_template.json 


## SidO Metadata dataplan templates
De metadata templates zijn in RDF aangeleverd. Locatie: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/definitions/sido.rdf

## SidO FAIR-Data Evaluator Assets
De SidO evaluatoren zijn in Ruby aangeleverd. Locatie: https://gitlab.com/fair-data-communities/sido/sido-evaluator 

## SidO FAIR-Data Evaluator Planning
De SidO evaluatoren zijn in Ruby aangeleverd. Locatie: https://gitlab.com/fair-data-communities/sido/sido-evaluator 

## Open Source Licentie
De licentie wordt op volgende locatie beschreven: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/LICENSE 

## Gebruikerslicentie
Alle data-aanbieders en datagebruikers binnen de SidO FAIR-Data Community gaan met de volgende gebruikerslicentie akkoord: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/licenses/user_license.md

## Community Beheer
Het community beheer van deze open source assets, geintroduceerd voor de FAIR-Data Community Samenwerken in de Ondergrond (SidO), wordt gedaan door de Purple Polar Bear B.V.

## Contributie
Zie de contributing file: https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/CONTRIBUTING

## Versies
De officiele versies worden gekenmerkt door tags die het formaat hebben van: release_jaartal. Daarnaast bevat master altijd de laatste gereleasde versie. Doorlopende ontwikkelingen vinden plaats in de development branch.


