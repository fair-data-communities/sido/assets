# Onderdelen

## Templates
1. Voor het implementeren van de data aanbiedende kant worden de onderdelen gebruikt die hieronder bij 2 benoemd worden.
2. De volgende templates zijn nodig:
  1. SidO Data Plan Template. Deze kan gevonden worden op https://gitlab.com/fair-data-communities/sido/assets/-/tree/master/dataplan_templates
  2. SidO Metadata Templates. Deze kunnen gevonden worden op https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/definitions/sido.rdf
3. De aangeboden assets data worden gecheckt op bruikbaarheid middels de SidO FAIR-Data Evaluator assets en planningen.

### FAIR-Score Assets
1. De aangeboden assets data worden gecheckt op bruikbaarheid middels de SidO
FAIR-Data Evaluator assets
2. De FAIR-score van deze evaluatie moet minstens XX% zijn, zodat de rest van de
community de data goed kan interpreteren.
3. De FAIR-score moet erkend worden door middel van een GO FAIR Foundation
certificaat.

### FAIR-Score Planningen
1. De aangeboden planningen data worden gecheckt op bruikbaarheid middels de SidO
FAIR-Data Evaluator planningen
2. De FAIR-score van deze evaluatie moet minstens XX% zijn, zodat de rest van de
community de data goed kan interpreteren.
3. De FAIR-score moet erkend worden door middel van een GO FAIR Foundation
certificaat.

### SidO FAIR-Data Evaluators
1. De SidO FAIR-Data Evaluator wordt jaarlijks vernieuwd.
2. Deelnemers moeten tenminste voldoen aan huidige versie – 2 jaar.
