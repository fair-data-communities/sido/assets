### CONCEPT

# Aansluitingsvoorwaarden Data Provider

### 1. Type organisatie
1. De organisatie is een waterbedrijf
1. De organisatie is een gemeenten
1. De organisatie is een netbeheerder
1. De organisatie is een waterbeheerder

* Er worden in de toekomst meer type organisaties aan deze lijst toegevoegd.

### 2. Aanvraag voor het totstandbrengen van de SidO FAIR-Data Community aansluiting

1. Een aanvraag voor het tot stand brengen van een aansluiting gebeurt via een FAIR-Data Community deelnemer (data provider) of het Centrum Ondergronds Bouwen (COB).
1. De personen zijn bevoegd om een aansluiting tot stand te brengen. 

### 3. Realisatie van de aansluiting

1. Voordat tot aansluiting kan worden overgegaan, kan de organisatie informatie inwinnen over de manier waarop de data onderling gedeeld wordt; FAIR-werkwijze 
1. De aansluiting tot de FAIR-Data Community wordt uitgevoerd met de daarvoor bestemde open source [onderdelen](https://gitlab.com/fair-data-communities/sido/assets/-/blob/master/licenses/onderdelen.md). Aan de hand daarvan kan de aansluiting succesvol voltooid worden en de data daadwerkelijk aansluiten op de rest van de FAIR-Data van de Community. 
1. De organisatie kan zelf bepalen via welke weg zij zich aansluiten bij de FAIR-Data Community. 
    1. Dit kan via een implementatie partner. Deze partner ondersteund de organisatie bij de aansluiting tot de SidO FAIR-Data Community. Tevens zal deze partner leden van de organisatie een stuk FAIR-Data context verschaffen.
    1. De aansluiting kan ook via de organisatie zelf gefaciliteerd worden, indien er genoeg kennis aanwezig is.

# Aansluitingsvoorwaarden Data Consumer

### 1. Type situatie
1. De organisatie is een data provider die de gedeelde data in één van haar viewers wil visualiseren of de gedeelde data in een applicatie wil tonen.
2. De organisatie is een software provider die is ingehuurd door de FAIR-Data Community of één van de data providers voor het maken van een applicatie die gebruikt maakt van de gedeelde data.

### 2. Aanvraag voor het gebruik (consumeren) van de data van de SidO FAIR-Data Community

1. Een aanvraag voor het gebruiken van de data van de SidO FAIR-Data Community gebeurt via een FAIR-Data Community deelnemer (data provider) of het Centrum Ondergronds Bouwen (COB).



