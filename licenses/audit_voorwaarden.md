### CONCEPT

# Audit voorwaarden data gebruiker (data consumer)

### Audit ten aanzien van datagebruik
De audit heeft enkel betrekking op het datagebruik van de gedeelde data binnen de SidO FAIR-Data Community. 

### Scope audit
De audit dient zich alleen bezig te houden met de wijze waarop de gedeelde data van de FAIR-Data Community gebruikt wordt. Dit gebruik dient in lijn te zijn met de gebruikersovereenkomst (user licence). De audit is sterk organisatorisch van aard. De audit streeft ernaar technisch non-invasief te zijn, auditors zijn niet geintresseerd in de techniek maar wel in het datagebruik en de organisatie aspecten die hieraan ten grondslag liggen. Indien nodig kan de auditor een NDA tekenen met betrekking tot technische onderwerpen.

##### Onderwerpen zoals:
1. Implementatie van de SidO FAIR-Connectoren
1. Implementatie, afwikkeling en onderhoud van data caching
1. Datalekken en de afhandeling hiervan
1. Data kopieën en het opslaan daarvan
1. Data delen met derden en de afhandeling hiervan

kunnen tijdens de audit aanbod komen. 

### Resultaten audit
Resultaten van de audit worden in eerste instantie voorgelegd aan de organisatie die geaudit wordt. Hierna zullen de resulaten aan het SidO FAIR-Data Bestuur overhandigd worden. Het bestuur kan hierna acteren op de resulaten. De mogelijke uitkomsten worden door het FAIR-Data Community bestuur onderbouwd.

### Uitvoerend orgaan audit
Het uitvoerend audit orgaan wordt door het FAIR-Data Community bestuur aangewezen op het moment dat een incident zich voordoet. 

### Geen liability

<!-- ? -->
